### EQONEX API Clients


#### Introduction

Some API clients to make connecting to the API more straight-forward.

If you have git installed, you can pip install directly from gitlab using the command `pip install git+https://gitlab.com/eqonex-ideas-forum/eqonex-api-clients.git`

Alternatively, clone the repo locally, then navigate to the folder and run `pip install .`

After this, you can import and run the REST, WebSocket and FIX client.


#### REST API


##### Creating a Client

Running a REST API Client for only public endpoints is very straightforward, for example:
```
from eqonex.eqonex_api_client_rest import EqonexApiClientRest
client = EqonexApiClientRest()
client.getOrderBook(pair_id=52)
```
will return the orderbook.

The client takes a `testnet` parameter that determines whether to connect to the prod or the testnet REST API.

To determine what endpoints are available, you can consult the code in the repo or use the python `dir()` command, eg. running the following (after executing the code above):

```
dir(client)
```
will return a list of the methods available, and

```
help(client.getTransferHistory)
```
will inform you of the data required to call the given endpoint.


##### Private endpoints

To access private endpoints in the REST API Client, you must provide a username and a password (and a 2FA code if you have 2FA enabled) when you create the client:
```
from eqonex.eqonex_api_client_rest import EqonexApiClientRest

testnet = False
username = "jack.jones@gmail.com"
password = "******"
code_2fa = "******"

client = EqonexApiClientRest(username=username, password=password, code_2fa=code_2fa, testnet=testnet)
client.getPositions()
```


##### Subaccounts

The REST API Client can also access subaccounts.  For all private endpoints, you can pass in a `subaccount_id` parameter that determines which subaccount to run the query on. As long as your main account has permission to view this subaccount, it will load the data for that subaccount.

```
client.getPositions(subaccount_id=1234)
```


##### Creating Client with API Key

A client can be created directly from an API Key if you don't want to pass through MFA every time you log in, using the class EqonexApiClientRestKeyAuth. Your API Key can be obtained and reset from your account management pages.

```
testnet = False
request_token = 'A1b2C3d4'
request_secret = 'Z1y2X3w4'
account_id = 12345

client_key = EqonexApiClientRestKeyAuth(requestToken=request_token,
                                        requestSecret=request_secret,
                                        accountId=account_id,
                                        testnet=testnet)
```

Be very careful about sharing your key with other users. Anyone who has your API Key has full trading access to your account. Reset your API Key immediately if you suspect anyone else has gained access.


#### WebSocket API


##### Creating a Client

The WebSocket API Client connects to the WebSocket 1.1 API and relies on the concept of subscriptions. After creating the client, you must inform the API which channels you're interested in recieving. The client runs a thread that will listen to the API and capture any updates that the API publishes in that channel. It also handles the heartbeat messages that the WebSocket server expects to recieve in order to keep channels open.

Create the client as follows:

```
from eqonex import EqonexApiClientWs

client_ws = EqonexApiClientWs()
```
and you should see `WebSocket Connection Opened` once the client has successfully connected to EQONEX.

This client is able to subscribe to public channels using the `subscribeOrderBook()` method and similar:
```
client_ws.subscribeOrderBook(symbols=['BTC/USDC[F]', 'ETH/USDC[F]'])
```

The client will now listen on the Order Book channel, and recieve updates. You can see the latest message by calling `getOrderBook()`:
```
client_ws.getOrderBook()
```

For channels like Order Book, the messages will be broken out by instrument. Each message recieved is stored as a tuple of `(message, timestamp)` to allow you can check for staleness.

The client takes a `testnet` parameter that determines whether to connect to the prod or the testnet WebSocket API.


##### Private endpoints

Private channels can also be subscribed to by providing a username and password when creating the client, which will then be able to subscribe to private channels:
```
from eqonex import EqonexApiClientWs

username = "jack.jones@gmail.com"
password = "******"

client_ws = EqonexApiClientWs(username=username, password=password)
client_ws.subscribePositions()
```
and recieved data can be fetched as before
```
client_ws.getPositions()
```


##### Subaccounts

When creating a WebSocket API Client with private endpoint permissions, an additional `subaccount_id` parameter can be passed in. As long as your main account has permission to view this subaccount, the WebSocket API Client will subscribe to updates from that subaccount instead of your main account:
```
from eqonex import EqonexApiClientWs

username = "jack.jones@gmail.com"
password = "******"
subaccount_id = 1234

client_ws = EqonexApiClientWs(username=username, password=password, subaccount_id=subaccount_id)
client_ws.subscribePositions()
```
and recieved data for the subaccount can be fetched as before
```
client_ws.getPositions()
```


##### Creating Client with API Key

A client can be created directly from an API Key if you don't want to use your username and password to log in, using the class EqonexApiClientWsKeyAuth. Your API Key can be obtained and reset from your account management pages. Note that when a WS client is created with an API key, an account or subaccount ID *MUST* be provided.

```
testnet = False
request_token = 'A1b2C3d4'
request_secret = 'Z1y2X3w4'
account_id = 12345

client_ws_key = EqonexApiClientWsKeyAuth(api_key=request_token,
                                         api_secret=request_secret,
                                         subaccount_id=account_id,
                                         testnet=testnet)
```

Be very careful about sharing your key with other users. Anyone who has your API Key has full trading access to your account. Reset your API Key immediately if you suspect anyone else has gained access.


#### FIX API

Instructions coming soon
