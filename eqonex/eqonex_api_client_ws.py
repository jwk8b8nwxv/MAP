from typing import Union, Dict, Any, Optional, List
import websocket
import threading
import json
import time
import hashlib
import hmac
from abc import ABC, abstractmethod


class EqonexApiClientWsBase(ABC):
    def __init__(self, verbose, testnet):
        self.base_endpoint = 'wss://{}eqonex.com/wsapi'.format("testnet." if testnet else "")
        self.connected = False
        self.verbose = verbose

        self.ws = websocket.WebSocketApp(self.base_endpoint,
                                         on_open=self.__on_open,
                                         on_close=self.__on_close,
                                         on_message=self.__on_message)

        self.ws_thread = threading.Thread(target=lambda: self.ws.run_forever())
        self.ws_thread.daemon = True
        self.ws_thread.start()

        # Keep track of the channels we have subscribed to, and the most recent message on the channel
        self.subscriptions = {1: False,    # Order Book
                              2: False,    # Order Book + Updates
                              3: False,    # Trades
                              4: False,    # Chart
                              5: True,     # Ticker - subscribe by default
                              6: False,    # Orders              (Private)
                              7: False,    # Positions           (Private)
                              8: False,    # Risk                (Private)
                              9: False}    # Available Balance   (Private)

        self.messages = {1: {}, 2: {}, 3: {}, 4: {}, 5: {}, 6: {}, 7: {}, 8: {}, 9: {}, "Default": {}}

    @abstractmethod
    def credentials_string(self, request_id, types):
        pass

    def __heartbeat(self) -> None:
        while True:
            time.sleep(20)
            heartbeat_message = '{"heartbeat":' + str(int(1000*time.time())) + '}'
            self.ws.send(heartbeat_message)

            if self.verbose:
                print(heartbeat_message)

    def __on_open(self,
                  wsapp: Any) -> None:
        print("WebSocket Connection Opened")
        self.connected = True

        subscribe_data = {
            "requestId": "Subscribe",
            "event": "S",
            "types": [5]
        }

        subscribe_message = json.dumps(subscribe_data, separators=(',', ':'))
        wsapp.send(subscribe_message)
        threading.Thread(target=self.__heartbeat).start()

        if self.verbose:
            print(subscribe_message)

    def __on_close(self,
                   wsapp: Any,
                   message: Any) -> None:
        print("WebSocket Connection Closed")
        unsubscribe_message = '{"unsubscribe": 1}'
        wsapp.send(unsubscribe_message)

    def __on_message(self,
                     wsapp: Any,
                     message: Any) -> Dict[Any, Any]:
        message_json = json.loads(message)

        # For endpoints split by pairId, let's keep all of the relevant entries
        if 'pairId' in message_json:
            self.messages[message_json.get("type", "Default")][message_json['pairId']] = (message, time.time())
        else:
            self.messages[message_json.get("type", "Default")] = (message, time.time())

    def __return_subscription(self,
                              channel : int = 5) -> Dict[Any, Any]:
        if not self.subscriptions[channel]:
            raise KeyError("Not subscribed to channel - subscribe to channel first")

        return self.messages[channel]

    def __subscription(self, subscribe_message):
        # Send the subscription message, but check that our WS is open first
        while not self.connected:
            time.sleep(1)

        self.ws.send(subscribe_message)

    def _credentials_string(self, request_id, types):
        pass

    #
    #    Public Channels
    #

    def subscribeOrderBook(self,
                           symbols: List[str] = ["BTC/USDC"],
                           level : int = None) -> None:
        self.subscriptions[1] = True
        subscribe_data = {"requestId": "orderbook", "event": "S", "types": [1], "symbols": symbols}
        if level is not None:
            subscribe_data["level"] = level
        subscribe_message = json.dumps(subscribe_data)
        self.__subscription(subscribe_message)

    def getOrderBook(self):
        return self.__return_subscription(1)

    def subscribeTradeHistory(self,
                              symbols: List[str] = ["BTC/USDC"]) -> None:
        self.subscriptions[3] = True
        symbols_string = '","'.join(symbols)
        subscribe_message = '{"requestId":"tradehistory","event":"S","types":[3],"symbols":["' + symbols_string + '"]}'
        self.__subscription(subscribe_message)

    def getTradeHistory(self):
        return self.__return_subscription(3)

    def subscribeCharts(self,
                        symbols: List[str] = ["BTC/USDC"],
                        timespan: Union[str, int] = 1) -> None:
        self.subscriptions[4] = True
        symbols_string = '","'.join(symbols)
        subscribe_message = '{"requestId":"chart","event":"S","types":[4],"timespan":' + str(timespan) + ',"symbols":["' + symbols_string + '"]}'
        self.__subscription(subscribe_message)

    def getCharts(self):
        return self.__return_subscription(4)

    def getTickers(self):
        return self.__return_subscription(5)

    #
    #    Private Channels
    #

    def subscribeOrders(self,
                        isInitialSnap : bool = True) -> None:
        if not self.logged_in:
            raise KeyError("Can't subscribe to Orders channel without providing login details")

        credentials_string = self.credentials_string("orders", [6])
        self.subscriptions[6] = True
        initial_snap_string = "true" if isInitialSnap else "false"

        subscribe_message = '{"requestId":"orders","event":"S","types":[6],' + credentials_string + \
            ',"isInitialSnap":"' + initial_snap_string + '"}'

        self.__subscription(subscribe_message)

    def getOrders(self):
        return self.__return_subscription(6)

    def subscribePositions(self) -> None:
        if not self.logged_in:
            raise KeyError("Can't subscribe to Positions channel without providing login details")

        credentials_string = self.credentials_string("positions", [7])
        self.subscriptions[7] = True

        subscribe_message = '{"requestId":"positions","event":"S","types":[7],' + credentials_string + '}'

        self.__subscription(subscribe_message)

    def getPositions(self):
        return self.__return_subscription(7)

    def subscribeRisk(self) -> None:
        if not self.logged_in:
            raise KeyError("Can't subscribe to Risk channel without providing login details")

        credentials_string = self.credentials_string("risk", [8])
        self.subscriptions[8] = True

        subscribe_message = '{"requestId":"risk","event":"S","types":[8],' + credentials_string + '}'

        self.__subscription(subscribe_message)

    def getRisk(self):
        return self.__return_subscription(8)

    def subscribeAvailableBalance(self) -> None:
        if not self.logged_in:
            raise KeyError("Can't subscribe to Available Balance channel without providing login details")

        credentials_string = self.credentials_string("availableBalance", [9])
        self.subscriptions[9] = True

        subscribe_message = '{"requestId":"availableBalance","event":"S","types":[9],' + credentials_string + '}'

        self.__subscription(subscribe_message)

    def getAvailableBalance(self):
        return self.__return_subscription(9)


class EqonexApiClientWsKeyAuth(EqonexApiClientWsBase):
    def __init__(self,
                 api_key: str = None,
                 api_secret: str = None,
                 subaccount_id : Optional[Union[int, str]] = None,
                 testnet: bool = False,
                 verbose: bool = False) -> None:
        self.logged_in = False

        if api_key is not None and api_secret is not None:
            self.api_key = api_key
            self.api_secret = api_secret
            self.logged_in = True

            if subaccount_id is not None:
                self.subaccount_id = str(subaccount_id)
            else:
                raise KeyError("Permissioned WS endpoints now REQUIRE an account ID")

        super().__init__(verbose, testnet)


    def credentials_string(self, request_id, types):
        if len(types) > 1:
            raise KeyError("Multi-subscriptions no longer supported")

        request_string = "S" + str(self.subaccount_id) + str(types[0]) + request_id
        signature_hash = hmac.new(self.api_secret.encode(), request_string.encode(), hashlib.sha384).hexdigest()

        credentials_string = '"username":"' + self.api_key + '","password":"' + signature_hash + '"'
        credentials_string += ',"account":"' + self.subaccount_id + '","useApiKeyForAuth":"true"'

        return credentials_string


class EqonexApiClientWs(EqonexApiClientWsBase):
    def __init__(self,
                 username: str = None,
                 password: str = None,
                 code_2fa: Union[int, str] = None,
                 subaccount_id : Optional[Union[int, str]] = None,
                 testnet: bool = False,
                 verbose: bool = False) -> None:
        from warnings import warn
        warn("EqonexApiClientWs is deprecated, please consider switching to the newer websocket client EqonexApiClientWsKeyAuth")

        self.logged_in = False

        if username is not None and password is not None:
            self.username = username
            self.password = password
            self.subaccount_id = None
            self.logged_in = True

            if subaccount_id is not None:
                self.subaccount_id = str(subaccount_id)

        super().__init__(verbose, testnet)

    def credentials_string(self, request_id, types):
        credentials_string = '"username":"' + self.username + '","password":"' + self.password + '"'

        if self.subaccount_id is not None:
            credentials_string = credentials_string + ',"account":"' + self.subaccount_id + '"'

        return credentials_string
