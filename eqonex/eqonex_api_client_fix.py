from typing import Union, Any, Callable, Optional, List
import ssl
import socket
import datetime
import time
import simplefix
import threading
import random
import string
import pandas as pd

def make_request_id(N=6):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=N))

class EqonexApiClientFix:
    def __init__(self,
                 host: str,
                 port: str,
                 sender_comp_id: Union[str, int],
                 target_comp_id: str,
                 username: str,
                 password: str,
                 heartbeat_seconds: int = 4,
                 verbose: bool = False) -> None:
        self._host = host
        self._port = port
        self._sender_comp_id = sender_comp_id
        self._target_comp_id = target_comp_id
        self._username = username
        self._password = password

        self._heartbeat_seconds = heartbeat_seconds
        self._verbose = verbose

        self._next_send_seq_num = 1
        self.messages = []
        self.parser = simplefix.FixParser()

        #Establish socket and prepare for login
        context = ssl.create_default_context()
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #s.ioctl(socker.SIO_KEEPALIVE_VALS, (1, 3000, 1000)) # Windows only...
        self.sock = context.wrap_socket(s, server_hostname=self._host)        

        #self.run()

    def login(self) -> None:
        self.sock.connect((self._host, self._port))

        if self._verbose:
            print("{}:{}".format(self._host, self._port))
            print()
            print("===CONNECTED===")
            print()

        self.login_message()

        # Kickstart a regular heartbeat in a seperate thread
        self.heartbeat_thread()

    def send_message(self,
                     message: Any) -> None:
        if self._verbose:
            print("<< {0}".format(message.encode().decode('utf-8').replace('\x01', '|').replace(self._password, "******")))

        self.sock.sendall(message.encode())
        self._next_send_seq_num += 1

    def send_and_receive_message(self,
                                 message: Any,
                                 callback_func: Callable,
                                 callback_kwargs: Any = {}) -> Any:
        self.send_message(message)

        data = self.sock.recv(5000)
        self.parser.append_buffer(data)
        msg = self.parser.get_message()
        self.messages.append(msg)

        if self._verbose:
            print(">> {0}".format(msg))

        # If we got a heartbeat back, respond with heartbeat and resend message
        if msg.get(simplefix.TAG_MSGTYPE) == simplefix.MSGTYPE_HEARTBEAT:
            self.send_heartbeat()
            msg = callback_func(**callback_kwargs)

        # If we got a resent request, resend the message
        if msg.get(simplefix.TAG_MSGTYPE) == simplefix.MSGTYPE_RESEND_REQUEST:
            msg = callback_func(**callback_kwargs)

        return msg

    def send_heartbeat(self) -> None:
        msg = simplefix.FixMessage()

        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_HEARTBEAT)
        msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.4')
        msg.append_pair(simplefix.TAG_SENDER_COMPID, self._sender_comp_id)
        msg.append_pair(simplefix.TAG_TARGET_COMPID, self._target_comp_id)
        msg.append_pair(simplefix.TAG_MSGSEQNUM, self._next_send_seq_num)
        msg.append_utc_timestamp(simplefix.TAG_SENDING_TIME)

        self.sock.sendall(msg.encode())
        self._next_send_seq_num += 1

        if self._verbose:
            print("<< {0}".format(msg.encode().decode('utf-8').replace('\x01', '|')))

    def heartbeat_thread(self) -> None:
        threading.Timer(self._heartbeat_seconds, self.heartbeat_thread).start()
        self.send_heartbeat()

    def login_message(self) -> Any:
        msg = simplefix.FixMessage()

        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_LOGON)
        msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.4')
        msg.append_pair(simplefix.TAG_SENDER_COMPID, self._sender_comp_id)
        msg.append_pair(simplefix.TAG_TARGET_COMPID, self._target_comp_id)
        msg.append_pair(simplefix.TAG_MSGSEQNUM, self._next_send_seq_num)
        msg.append_utc_timestamp(simplefix.TAG_SENDING_TIME)

        msg.append_pair(553, self._username)
        msg.append_pair(554, self._password)
        msg.append_pair(simplefix.TAG_ENCRYPTMETHOD, 0)
        msg.append_pair(simplefix.TAG_HEARTBTINT, 5)
        msg.append_pair(simplefix.TAG_RESETSEQNUMFLAG, "Y")
        msg.append_pair(6867, "N")

        self._next_send_seq_num += 1

        return self.send_and_receive_message(msg, self.login_message)

    def market_data_request(self,
                            symbols: List[str] = ["BTC/USDC"]) -> Any:
        if "fix-md" not in self._target_comp_id:
            raise KeyError("market_data_request requires TargetCompId to contain fix-md.equos")

        msg = simplefix.FixMessage()

        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_MARKET_DATA_REQUEST)
        msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.4')
        msg.append_pair(simplefix.TAG_SENDER_COMPID, self._sender_comp_id)
        msg.append_pair(simplefix.TAG_TARGET_COMPID, self._target_comp_id)
        msg.append_pair(simplefix.TAG_MSGSEQNUM, self._next_send_seq_num)
        msg.append_utc_timestamp(simplefix.TAG_SENDING_TIME)

        msg.append_pair(262, make_request_id()) # MDReqID
        msg.append_pair(263, '1')  # SubscriptionRequestType
        msg.append_pair(264, 0)    # MarketDepth
        msg.append_pair(265, 0)    # MDUpdateType
        msg.append_pair(266, 'Y')  # AggregatedBook

        msg.append_pair(267, 2)    # NoMDEntryTypes
        msg.append_pair(269, '0')  # MDEntryType
        msg.append_pair(269, '1')  # MDEntryType

        msg.append_pair(146, len(symbols))    # NoRelatedSym
        for symbol in symbols:
            msg.append_pair(simplefix.TAG_SYMBOL, symbol)

        return self.send_and_receive_message(msg, self.market_data_request, {"symbols": symbols})

    def pretty_market_data_request(self,
                                   symbols: List[str] = ["BTC/USDC"]) -> Any:
        msg = self.market_data_request(symbols)
        return EqonexApiClientFix.parse_market_data(msg)

    def security_list_request(self) -> Any:
        if "fix-md" not in self._target_comp_id:
            raise KeyError("security_list_request requires TargetCompId to contain fix-md.equos")

        msg = simplefix.FixMessage()

        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_SECURITY_LIST_REQUEST)
        msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.4')
        msg.append_pair(simplefix.TAG_SENDER_COMPID, self._sender_comp_id)
        msg.append_pair(simplefix.TAG_TARGET_COMPID, self._target_comp_id)
        msg.append_pair(simplefix.TAG_MSGSEQNUM, self._next_send_seq_num)
        msg.append_utc_timestamp(simplefix.TAG_SENDING_TIME)

        msg.append_pair(320, make_request_id()) # SecurityReqID
        msg.append_pair(559, 4)                 # SecurityListRequestType

        return self.send_and_receive_message(msg, self.security_list_request)

    def pretty_security_list_request(self) -> Any:
        msg = self.security_list_request()
        return EqonexApiClientFix.parse_security_list(msg)

    def new_order(self,
                  account: Union[str, int],
                  symbol: str,
                  side: int,
                  quantity: Optional[Union[int, float]],
                  order_type: int = 2,
                  price: Optional[Union[int, float]] = None,
                  time_in_force: int = 0,
                  client_order_id: Union[str, int] = None) -> Any:
        if "fix-om" not in self._target_comp_id:
            raise KeyError("new_order requires TargetCompId to contain fix-om.equos")

        if client_order_id is None:
            client_order_id = make_request_id()

        msg = simplefix.FixMessage()

        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_NEW_ORDER_SINGLE)
        msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.4')
        msg.append_pair(simplefix.TAG_SENDER_COMPID, self._sender_comp_id)
        msg.append_pair(simplefix.TAG_TARGET_COMPID, self._target_comp_id)
        msg.append_pair(simplefix.TAG_MSGSEQNUM, self._next_send_seq_num)
        msg.append_utc_timestamp(simplefix.TAG_SENDING_TIME)

        msg.append_pair(simplefix.TAG_ACCOUNT, account)
        msg.append_pair(simplefix.TAG_CLORDID, client_order_id)
        msg.append_pair(simplefix.TAG_ORDERQTY, quantity)
        msg.append_pair(simplefix.TAG_ORDTYPE, order_type)

        if price is not None:
            msg.append_pair(simplefix.TAG_PRICE, price)
        else:
            msg.append_pair(simplefix.TAG_PRICE, 0)

        msg.append_pair(simplefix.TAG_SIDE, side)
        msg.append_pair(simplefix.TAG_SYMBOL, symbol)
        msg.append_pair(simplefix.TAG_TIMEINFORCE, 0)

        time_str = str(datetime.datetime.utcnow()).replace("-","").replace(" ", "-")[:-3]
        msg.append_pair(simplefix.TAG_TRANSACTTIME, time_str)

        return self.send_message(msg)

    def cancel_order(self,
                     account: Union[str, int],
                     client_order_id: Union[str, int],
                     original_client_order_id: Union[str, int],
                     side: int,
                     symbol: str) -> Any:
        if "fix-om" not in self._target_comp_id:
            raise KeyError("cancel_order requires TargetCompId to contain fix-om.equos")

        msg = simplefix.FixMessage()

        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_ORDER_CANCEL_REQUEST)
        msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.4')
        msg.append_pair(simplefix.TAG_SENDER_COMPID, self._sender_comp_id)
        msg.append_pair(simplefix.TAG_TARGET_COMPID, self._target_comp_id)
        msg.append_pair(simplefix.TAG_MSGSEQNUM, self._next_send_seq_num)
        msg.append_utc_timestamp(simplefix.TAG_SENDING_TIME)

        msg.append_pair(simplefix.TAG_ACCOUNT, account)
        msg.append_pair(simplefix.TAG_CLORDID, client_order_id)
        msg.append_pair(simplefix.TAG_ORIGCLORDID, original_client_order_id)
        msg.append_pair(simplefix.TAG_SIDE, side)
        msg.append_pair(simplefix.TAG_SYMBOL, symbol)

        time_str = str(datetime.datetime.utcnow()).replace("-","").replace(" ", "-")[:-3]
        msg.append_pair(simplefix.TAG_TRANSACTTIME, time_str)

        callback = lambda: self.cancel_order(account, client_order_id, original_client_order_id, side, symbol)
        return self.send_and_receive_message(msg, callback)

    def order_mass_status_request(self,
                                  mass_client_request_id: Union[str, int],
                                  mass_status_request_type: int = 7,
                                  symbol: str = None) -> Any:
        # NB. Currently returning |58=Unsupported Message Type|
        if "fix-om" not in self._target_comp_id:
            raise KeyError("order_mass_status_request requires TargetCompId to contain fix-om.equos")

        if mass_client_request_id is None:
            mass_client_request_id = make_request_id()

        msg = simplefix.FixMessage()

        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_ORDER_MASS_STATUS_REQUEST)
        msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.4')
        msg.append_pair(simplefix.TAG_SENDER_COMPID, self._sender_comp_id)
        msg.append_pair(simplefix.TAG_TARGET_COMPID, self._target_comp_id)
        msg.append_pair(simplefix.TAG_MSGSEQNUM, self._next_send_seq_num)
        msg.append_utc_timestamp(simplefix.TAG_SENDING_TIME)

        msg.append_pair(584, mass_client_request_id)
        msg.append_pair(585, mass_status_request_type)

        if symbol is not None:
            msg.append_pair(simplefix.TAG_SYMBOL, symbol)

        time_str = str(datetime.datetime.utcnow()).replace("-","").replace(" ", "-")[:-3]
        msg.append_pair(simplefix.TAG_TRANSACTTIME, time_str)

        callback = lambda: self.order_mass_status_request(mass_client_request_id, mass_status_request_type, symbol)
        return self.send_and_receive_message(msg, callback)

    def order_mass_cancel_request(self,
                                  account: Union[str, int],
                                  mass_client_cancel_id: Union[str, int],
                                  mass_cancel_request_type: int = 7,
                                  symbol: str = None) -> Any:
        if "fix-om" not in self._target_comp_id:
            raise KeyError("order_mass_cancel_request requires TargetCompId to contain fix-om.equos")

        if mass_client_cancel_id is None:
            mass_client_cancel_id = make_request_id()

        msg = simplefix.FixMessage()

        msg.append_pair(simplefix.TAG_MSGTYPE, simplefix.MSGTYPE_ORDER_MASS_CANCEL_REQUEST)
        msg.append_pair(simplefix.TAG_BEGINSTRING, 'FIX.4.4')
        msg.append_pair(simplefix.TAG_SENDER_COMPID, self._sender_comp_id)
        msg.append_pair(simplefix.TAG_TARGET_COMPID, self._target_comp_id)
        msg.append_pair(simplefix.TAG_MSGSEQNUM, self._next_send_seq_num)
        msg.append_utc_timestamp(simplefix.TAG_SENDING_TIME)

        msg.append_pair(simplefix.TAG_ACCOUNT, account)
        msg.append_pair(simplefix.TAG_CLORDID, mass_client_cancel_id)
        msg.append_pair(530, mass_cancel_request_type)

        if symbol is not None:
            msg.append_pair(simplefix.TAG_SYMBOL, symbol)

        time_str = str(datetime.datetime.utcnow()).replace("-","").replace(" ", "-")[:-3]
        msg.append_pair(simplefix.TAG_TRANSACTTIME, time_str)

        callback = lambda: self.order_mass_cancel_request(account, mass_client_cancel_id, mass_cancel_request_type, symbol)
        return self.send_and_receive_message(msg, callback)

    @staticmethod
    def parse_security_list(message):
        if message.get(simplefix.TAG_MSGTYPE) != simplefix.MSGTYPE_SECURITY_LIST:
            raise KeyError("Parsed message is not a security list")

        n_securities = int(message.get(146))
        results_data = []
        columns = ['symbol', 'CFICode', 'security description', 'instrument price precision', 'minimum price increment',
                   'minimum price increment amount', 'base currency', 'contract currency', 'settlement currency',
                   'commission currency', 'roundlot', 'min trade vol', 'max trade vol']

        for i in range(n_securities):
            idx = i + 1
            data = []

            data.append(message.get(simplefix.TAG_SYMBOL, idx).decode('utf-8'))
            data.append(message.get(461, idx).decode('utf-8'))
            data.append(message.get(simplefix.TAG_SECURITYDESC, idx).decode('utf-8'))
            data.append(float(message.get(2576, idx)))
            data.append(float(message.get(969, idx)))
            data.append(float(message.get(1146, idx)))
            data.append(message.get(simplefix.TAG_CURRENCY, idx).decode('utf-8'))
            data.append(message.get(521, idx).decode('utf-8'))  # ContAmtCurr
            data.append(message.get(simplefix.TAG_SETTLCURRENCY, idx).decode('utf-8'))
            data.append(message.get(479, idx).decode('utf-8'))  # CommCurrency
            data.append(float(message.get(561, idx)))
            data.append(float(message.get(562, idx)))
            data.append(float(message.get(1140, idx)))

            results_data.append(data)

        return pd.DataFrame(results_data, columns=columns)

    @staticmethod
    def parse_market_data(message):
        valid_responses = [simplefix.MSGTYPE_MARKET_DATA_SNAPSHOT_FULL_REFRESH,
                           simplefix.MSGTYPE_MARKET_DATA_INCREMENTAL_REFRESH,
                           simplefix.MSGTYPE_MARKET_DATA_REQUEST_REJECT]

        if message.get(simplefix.TAG_MSGTYPE) not in valid_responses:
            raise KeyError("Parsed message is not a market data request response")

        results_data = []

        if message.get(simplefix.TAG_MSGTYPE) == simplefix.MSGTYPE_MARKET_DATA_SNAPSHOT_FULL_REFRESH:
            symbol = message.get(simplefix.TAG_SYMBOL).decode('utf-8')
            instrument_id = message.get(simplefix.TAG_SECURITY_ID).decode('utf-8')
            id_source = message.get(simplefix.TAG_SECURITYIDSOURCE).decode('utf-8')

            n_records = int(message.get(268))
            columns = ['type', 'price', 'size']

            for i in range(n_records):
                idx = i + 1
                data = []

                data.append(message.get(269, idx).decode('utf-8'))  # Type (0=Bid, 1=Offer)
                data.append(float(message.get(270, idx)))  # Price
                data.append(float(message.get(271, idx)))  # Size

                results_data.append(data)

        elif message.get(simplefix.TAG_MSGTYPE) == simplefix.MSGTYPE_MARKET_DATA_INCREMENTAL_REFRESH:
            n_records = int(message.get(268))
            columns = ['update action', 'delete reason', 'symbol', 'instrument id', 'security id source',
                       'type', 'price', 'size']

            for i in range(n_records):
                idx = i + 1
                data = []

                data.append(message.get(279, idx).decode('utf-8'))  # Update Action (0=New, 1=Change, 2=Delete)
                data.append(message.get(285, idx))  # Delete Reason (0=Cancellation, 1=Error)
                data.append(message.get(simplefix.TAG_SYMBOL, idx).decode('utf-8'))
                data.append(message.get(simplefix.TAG_SECURITY_ID, idx).decode('utf-8'))
                data.append(message.get(simplefix.TAG_SECURITYIDSOURCE, idx).decode('utf-8'))
                data.append(message.get(269, idx).decode('utf-8'))  # Type (0=Bid, 1=Offer)
                data.append(float(message.get(270, idx)))  # Price
                data.append(float(message.get(271, idx)))  # Size

                results_data.append(data)

        else:
            # Request Reject - what to return?
            return None

        return pd.DataFrame(results_data, columns=columns)
